let marks=[34,92,54,78,99];
let fruits=['apple', 'banana', 'mango'];
let mixArry=['one', 2, [3,4]];
let arr=[];
arr.length=100;

console.log(marks);
console.log(fruits);
console.log(mixArry);
console.log(arr);

//methods 
console.log(`Index: ${marks.indexOf(78)}`)               // gives index of particular element
marks.push(35);                                          // append element at the end of array i.e push is used to mutate/modify array
console.log(`Marks:  ${marks}`);
// marks.unshift(100);                                      // add element at the begining of array
// console.log(`Marks:  ${marks}`);
// marks.pop();                                             // Removes the last element from an array and returns it
// console.log(`Marks:  ${marks}`);
// marks.shift();                                           // Removes the first element from an array and returns it. 
// console.log(`Marks:  ${marks}`);
// marks.splice(3,2)                                        // Removes elements from particular index to number of elemets to be deleted
// console.log(`Marks:  ${marks}`);
// marks.reverse();                                           // Reverse the array and return it.
// console.log(`Marks:  ${marks}`);
marks= marks.concat(mixArry);
console.log(`Marks:  ${marks}`);