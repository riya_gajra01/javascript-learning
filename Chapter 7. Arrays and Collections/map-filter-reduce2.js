const users=[
    { firstName: "Riya", lastName: "Gajra", age: 23 },
    { firstName: "arijith", lastName: "Roy", age: 35 },
    { firstName: "Ravi", lastName: "Baranjalekar", age: 23 },
    { firstName: "Abhijit", lastName: "Bharambe", age: 34 },
];
// we want result as list of full names
// ["Riya Gajra", "Arijith Roy", "Ravi Baranjalekar"]

// let output =users.map(x=> `${x.firstName} ${x.lastName}`);
// console.log(output);

// we want list of people as aboject based on the age group 
// like {23: 2, 35:1, 34:1}

//a={}, curr=Riya Gajra 23
 // if 23 is present in a then a++
 //else assign a initially 1
output= users.reduce(function(acc,curr){
    if(acc[curr.age]){
        acc[curr.age]++;
    }else{
        acc[curr.age]=1;
    }
    return acc;
},{});
console.log(output);

//listout firstName of all the users whose age < 30
// like ["Riya", "Ravi"]
// output= users.filter((x)=> x.age<30);
// // output= users.filter((x)=> x.age<30).map((x)=>x.firstName);
// console.log(output);