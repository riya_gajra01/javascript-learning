// let names = new Array( 'Riya')
// console.log(names);
// console.log(names.length);

// names = Array('Riya', 'Ravi', 'Arijith')
// console.log(names);
// console.log(names.length);

// let marks=  Array(100);
// console.log(marks);
// console.log(marks.length);

// overcome the problem of above array constructor
let numArray= Array.of(100);
console.log(numArray);
console.log(numArray.length);