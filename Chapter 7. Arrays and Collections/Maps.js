//Maps in Js are similer to object.
// only diff is Object keys must be strings or symbols, but Map keys can be of any type.

const myMap= new Map();
 // console.log(myMap);
// console.log(Object.getPrototypeOf(myMap));

const key1= "myStr",
       key2= {},
          key3= function(){ return "hello"};

         
//settting map values
myMap.set(key1, "This is String");
myMap.set(key2, "This is an empty object");
myMap.set(key3, "This is a blank function");
console.log(myMap)

//gettting map values
// console.log(myMap);
// let value1=myMap.get(key1);
// let value2=myMap.get(key2);
 let value3=myMap.get(key3);
 console.log(value1+"\t\t"+value2+"\t\t"+value3)
 console.log(value3())

// iterate over map using for..of
// for(let[key] of myMap){
//     console.log(key);
// }

// // iterate using forEach
// myMap.forEach((key,value)=>{
//     console.log('Key is: '+key);
//     console.log('Value is: '+value);
// })