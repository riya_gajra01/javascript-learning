const arr= [5,1,3,2,6];
//Double=[10,2,6,4,12]
//Triple=[15,3,9,6,18]
//Binary=[101,1,11,10,110]
// function double(x){
//     return x*2;
// }
// function triple(x){
//     return x*3;
// }
// function binary(x){
//     return x.toString(2);
// }
// here map function is used to perform desired transformation. In our case the trasformation is just double the value of exsiting 
// array elements.
//let output= arr.map(double);
// output= arr.map(function binary(x){
//     return x.toString(2);
// });
// //  output= arr.map((x)=>
// //     x.toString(2)
// // );

// console.log(output);
 

//filter function filters the values based on the given logic. 
// function isOdd(x){
// return x%2!==0;
// }
// function isEven(x){
//     return x%2==0;
// }
//fiter array and return odd values only.
// output=arr.filter(isOdd);
//  console.log(output);

//reduce() is used when we have to take all of the elements of an array and come up with single value out of them.
// forex find sum of all elements or find max/min among all

// function findSum(arr){
//     let sum=0;
//     for (let i = 0; i< arr.length; i++) {
//      sum= sum+arr[i];
//     }
//     return sum;
// }
// console.log(findSum(arr));

// reduce uses two parameter i.e. reduce(function(acc, curr), initial value of accumulator)
// acc is accumulator, curr is current.
// current represent the current iterable elemenet of array whereas accumulator is the the expected ans which is going
// to be accumulated.
// output=arr.reduce((sum,i)=>{
//     sum=sum+i;
//     return sum;
// },0);
// console.log(output);


//find max in normal way
// function findMax(arr){
//     let max=0;
//     for (let i = 0; i< arr.length; i++) {
//      if(arr[i]>max){
//      max= arr[i] ;
//        }
//     }
//     return max;
// }
// console.log(findMax(arr));

// max using reduce
// output=arr.reduce((max,i)=>{
//    if(i>max){
//     max=i;
//    }
//     return max;
// },0);
// console.log(output);

const array = [1, 7, 2, 9]
const result = array.reduce((x, y) => x + y)
console.log(result);