// using function keyword
let sum= function(x,y){return x+y}
console.log(sum(2,3))

// using arraow operator
sum=(x,y)=> x+y
console.log(sum(8,3))
const num = () => Math.trunc(Math.random() * 6) + 1  // function with no parameters
console.log(num())

//function returns object literals
const stats = (x, y) => ({
    average: (x + y) / 2,
    distance: Math.abs(x - y)
  })
  console.log(stats(4,6))