const data = [45, 45, 45, 45, 45,45]
const div = d3.select('body')
const width = window.innerWidth
const height = window.innerHeight
const radius = Math.min(width,height) / 6
const colorScale= d3.scaleOrdinal(['#7326AB', '#58B6AB', '#E5A1D4', '#B66858','#D5E65D','#DC143C'])
const svg = div.append('svg')
    .attr('width', width)
    .attr('height', height)
    .append('g')
        .attr('transform',`translate(${width / 2}, ${height / 2})`)

const pie = d3.pie().value(d => d).sort(null)
const arc= d3.arc().outerRadius(radius).innerRadius(50)

const g= svg.selectAll('.arc')
    .data(pie(data))
    .enter().append('g')
    .attr('class', 'arc')

g.append('path')
    .attr('d', arc)
    .attr('class', 'arc')
    .style('fill', (d, i) => colorScale(i))
    .style('stroke', '#11141c')
    .style('stroke-width', 10)