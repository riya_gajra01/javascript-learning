// Symbol.species allows us to alter the default constructor in prototype methods of some built-in classes.

class MyPromise extends Promise{}

const a= new MyPromise(()=>{})

    console.log(a instanceof MyPromise)
    console.log(a instanceof Promise)
    console.log(a.constructor)

const b= a.then(() => {})
    console.log(b.constructor)
    // console.log(b)
/*
// here o/p is [class MyPromise extends Promise] means in JS
// the promise then method on the prototype will use the base
// target constructor.
// */
// console.log(MyPromise[Symbol.species]);
Object.defineProperty(MyPromise, Symbol.species, {
    value:Promise}
)
// const c= a.then(() => {})
//     console.log(c.constructor)
//    // console.log(c)
