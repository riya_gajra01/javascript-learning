// const idSyml= Symbol('id');
let user ={
    id   : 'NIPL124',
    name : 'Riya Gajr',
    city : 'Udgir',
    age  :  23
//  , idSyml: 124
}

const idSyml= Symbol('id');
user[idSyml] = 124;
console.log(user);

// user[Symbol.toString]  // here tostring return [object Object] property where as using toStringTag we can assign name to that object property
console.log(user.toString())
user[Symbol.toStringTag] = 'USER'
console.log(user.toString())
