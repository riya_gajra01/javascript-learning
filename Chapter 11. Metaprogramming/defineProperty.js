const object = {};
Object.defineProperty(object, 'property', {
  value: 42,
  writable: false
});

object.property = 77;
console.log(object.property);
/*
Object.defineProperty(obj, prop, descriptor)
Here, 
obj : The object on which to define the property.
prop : The name or Symbol of the property to be defined or modified.
    configurable ==> when this is set to false, the type of this property cannot be changed between data property and accessor property, 
    and the property may not be deleted, and other attributes of its descriptor cannot be changed 
    (however, if it's a data descriptor with writable: true, the value can be changed, and writable can be changed to false).
    Defaults to false.
    enumerable ==>  true if and only if this property shows up during enumeration of the properties on the corresponding object. Defaults to false.
descriptor : The descriptor for the property being defined or modified.
    value ==> The value associated with the property. Can be any valid JavaScript value (number, object, function, etc.). Defaults to undefined.
    writable ==> true if the value associated with the property may be changed with an assignment operator. Defaults to false.
*/