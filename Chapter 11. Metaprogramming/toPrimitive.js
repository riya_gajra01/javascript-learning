const object = {
    [Symbol.toPrimitive](hint) {
      //hint specifies the preferred type of the result primitive value. The hint can be one of "number", "string", and "default"
      if (hint === 'number') {
        return 42;
      }
      if (hint === 'string') {
        return 'hello';
      }
      return true;
    }
  };
  
  console.log(object);
  console.log(+object);
  console.log(object + '');
  console.log(`${object}`);
 