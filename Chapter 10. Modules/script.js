// //Normal Import
// import {default: User, printUser} from "./user.js"

// const user =new User("Riya", "Gajra");
// printUser(user);

//Dynamic Import
setTimeout(()=> {
    import("./user.js").then(({default: User, printUser})=>{
    const user =new User("Riya", "Gajra");
    printUser(user);
})
}, 1000)
