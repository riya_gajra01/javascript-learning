/*
ES6 Modules are used to keep the code clean and maintainable.
The main idea behind modules is allowing us to import and export different section of code from 
into other file which allows us to break apart the code into more smaller grained file which makes
our code easier to understand and easy to locate and alter later as per need.
*/

export default class User{
    constructor(firstName, lastName){
        this.firstName=firstName;
        this.lastName=lastName;
    }
}

// export function printFName(user) {
//     console.log(`User's first name is ${user.firstName}.` );
// }

// export function printLName(user) {
//     console.log(`User's surname is ${user.lastName}.` );
// }

export function printUser(user){
    console.log(`${user.firstName} ${user.lastName}`)
}