// import englishTranslation from "./en-translation.js"
// import spanishTranslation from "./sp-translation.js"
// import frenchTranslation from "./fr-translation.js"

//2nd use case: Having but of files among which you only want to download one based on the requirement. 

const user= {locale: "en"}
// let transalation
// switch (user.locale) {
//     case "sp":
//         transalation= spanishTranslation
//         break;
//     case "fr":
//         transalation= frenchTranslation
//         break;
//     default:
//         transalation= englishTranslation 
// }

import(`./${user.locale}-translation.js`)
.catch(()=> import('./en-translation.js'))
.then(({default: transalation})=>{
    console.log(transalation.HI)
})