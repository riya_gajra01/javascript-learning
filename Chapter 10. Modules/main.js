//IMPORT PART OF MODULE
import sum from './lib.js';
console.log(sum(5, 5)+"\n"); // 👉️ 10

import User,{printName, printAge}  from "./user.js";

const user= new User("Riya", 23);
console.log(user);

printName(user);
printAge(user);