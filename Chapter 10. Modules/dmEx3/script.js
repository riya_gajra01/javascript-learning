// import renderRectangle from "./rectangle.js"
// import renderTriangle from "./triangle.js"

const shapes= [
    { type: "rectangle" },
    { type: "triangle" },
    { type: "rectangle" }
]

// shapes.forEach(shape =>{
//     switch(shape.type){
//         case "rectangle":
//             renderRectangle(shape);
//             break;
//         case "triangle":
//             renderTriangle(shape);
//             break;
//     }
// })

shapes.forEach(shape =>{
    import(`./${shape.type}.js`).then(({default: render})=>{
        render(shape);
    })
})