
console.log(Object.prototype);
console.log(Object.getPrototypeOf(Object.prototype));
let b= {};
console.log(Object.getPrototypeOf(b));  // using getPrototypeOf() method we can check the prototype of object.
b= new Array();
console.log(Object.getPrototypeOf(b));
b= new String();
console.log(Object.getPrototypeOf(b));