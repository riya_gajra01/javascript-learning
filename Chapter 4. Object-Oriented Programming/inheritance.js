//superclass
var Mobile= function(){}

//prototyoe method of class Mobile
Mobile.prototype.getModel= function(){
    return this.model;
} 
var Samsung = function(model, price){
    this.model= model;
    this.price= price;
}
var Lenovo = function(model, price){
    this.model= model;
    this.price= price;
}

//inheritance
Samsung.prototype= Object.create(Mobile.prototype); // bcoz of this line, every prototype method of mobile class will be avilable for Samsung.
Samsung.prototype.constructor= Samsung;

Lenovo.prototype= Object.create(Mobile.prototype); 
Lenovo.prototype.constructor= Lenovo;

var galaxy= new Samsung('Galaxy A29', 27000);
console.log(galaxy.getModel());
