class Rectangle{
    constructor(width, height, color)
    {
        // setup method i.e. constructor
        console.log("Rectangle being created....");

        // instance properties
        this.width= width;
        this.height= height;
    }

      //instance methods
      getArea(){
          return this.width* this.height;
      }
}

let objRectangle= new Rectangle(3,5);
console.log(objRectangle);
console.log("Area of Rectangle: " +objRectangle.getArea());

console.log();

let objRectangle2= new Rectangle(10,5);
console.log(objRectangle2);
console.log("Area of Rectangle: " +objRectangle2.getArea());