class Square{

    //constructor
    constructor(width){
        this.width= width;
        this.height= width;
    }

    //getter
    get area(){
        return this.width * this.height;
    }

    //setter
    set area(area){
        this.width= Math.sqrt(area);
        this.height= this.width;
    }
}
//object
let square= new Square(25);
// console.log(square.area);

square.area=25;
console.log(square.area);
