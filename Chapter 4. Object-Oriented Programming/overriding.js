// overridding : a child class will use methods with the same name that are most commonly associated wuth its parent.
class Vehicle{
    run(){
        console.log("Vehicle is running....");
    }
}
class Car extends Vehicle{
    run(){
        console.log("Car is running....");
    } 
}
let car= new Vehicle();
let car1= new Car();
car.run();
car1.run();