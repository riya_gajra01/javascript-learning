let bonus;

class Employee {
   constructor(name, salary)
   {
    this.name= name;
    this.salary=salary;
   }
    getSalary() { 
        return this.salary;
    }
  }

  class Manager extends Employee {
   constructor(name, salary, bonus)
   {
    super();
    this.name= name;
    this.salary=salary;
    this.bonus=bonus;
   }
    getSalary() 
    { 
        return this.salary + this.bonus;
    }
  }
  let obj= new Employee('Riya',25000);
  console.log("Employee salary= "+obj.getSalary()); //invoke Employee.prototype.getSalary
console.log('-----------------------');
  obj= new Manager('Arijith', 90000, 10000);
  console.log("Manager salary= "+obj.getSalary()); //invoke Manager.prototype.getSalary
