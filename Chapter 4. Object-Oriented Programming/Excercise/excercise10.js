class BankAccount {
    constructor(balance){
        this.balance=balance;
        console.log(this.balance);
    }
    deposite(addBalance) {
        this.balance = this.balance + addBalance;
        console.log("Your Current Balance is "+this.balance);
    }
    withdraw(subBalance) {
        this.balance = this.balance - subBalance;
        console.log("Your Current Balance is "+this.balance);
    }

    myBalance(){
        console.log(this.balance);
        return this.balance;
    }
}

class SavingAccount extends BankAccount {
    
    constructor(balance){
        super(balance);
    }
    interestCalculate( time,rate) {
        this.interest = (this.balance * time * rate) / 100;
        this.addInterest();
    }
    addInterest() {
        this.balance=this.balance+this.interest;
        
    }
}

class CheckingAccount extends BankAccount {
    constructor(balance){
        super(balance);
    }
    withdrawOperation(amount) {
        this.balance = this.balance - 10;
        this.withdraw(amount);
    }
}

// const checkingAccount = new CheckingAccount(1000);
// checkingAccount.withdrawOperation(23);
// checkingAccount.deposite(200);

const saving = new SavingAccount(1000);
saving.interestCalculate(3,5);
console.log(saving.myBalance());



