class Random{
    static nextDouble(low, high){
      return low+(high-low)*Math.random()  
    }
    static nextInt(low, high){
        return low+ Math.trunc((high-low)*Math.random());
    }
    static nextElement(array){
        return array[Math.floor(Math.random()*array.length)];
    }
}
console.log("Random Double: "+Random.nextDouble(5,10));
console.log("Random Integer: "+Random.nextInt(5,10));
const arr= new Array(5,6,7,8,9);
console.log("Random Array Element: "+ Random.nextElement(arr));