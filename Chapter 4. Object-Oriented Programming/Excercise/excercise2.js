const createPointPrototype= {
   translate(){
        console.log(`Point moved from ${this.x} to ${this.y}`);
   },
    scale(scalePercentage){
       this.translate();
     console.log(`with ${scalePercentage} scalePercentage.`);
    }
 }
 function createPoint(x,y){
    const axis={x,y};
    Object.setPrototypeOf(axis, createPointPrototype);
    return axis;
 }
 const obj= new createPoint(25,50);
 obj.scale(0.25);