class Node {
    depth() { throw Error("abstract method") }
  }

 function Parent (value, children){
 depth= ()=> 1 + Math.max( children.map(n => n.depth()));  
 const P={value, children, depth};
 Object.setPrototypeOf(P, Node );
 return P;
 }
 
 function Leaf(value) {
    depth= ()=> 1;
    const L={value, depth};
    Object.setPrototypeOf(L, Node);
    return L;
  }

const parent=  new Parent(1, 1);
parent.depth();