const employeePrototype = {
    raiseSalary: function(percent) {
      this.salary *= 1 + percent / 100
    }
  }
  function createEmployee(name, salary) 
  {
    const result = { name, salary };
    Object.setPrototypeOf(result, employeePrototype);
    return result;
  }
var emp1= new createEmployee('Riya', 18000);
var emp2= new createEmployee('Ravi', 18000);
console.log(emp1)
emp1.raiseSalary(10);
console.log(emp1)