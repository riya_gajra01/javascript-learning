The static keyword is use to define a static method for a class. Static methods
are called without creating object and cannot be called through a class instance i.e. Object.
Static methods are mostly used to create utility functions for application.