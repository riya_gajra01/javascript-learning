// let students = [
//     {name: 'Alankar'},
//     {name: 'Rohan'}
// ];


/**
 * async way: Order of execution problem can't be manage
 **/ 

// function getAllStudents() {
//     let listTemplate = '';
//     setTimeout(() => {
//         students.forEach((e, i) => {
//             let listItem = `<li>${i+1}: ${e.name}</li>`;
//             listTemplate += listItem;
//         });
//         document.getElementById('list').innerHTML = listTemplate;
//     }, 1000);
// }

// function createStudent(name) {
//     setTimeout(() => {
//         students.push({name});
//     }, 3000);
// }

// createStudent('Amey');
// getAllStudents();


/**
 * callback way: order of execution can be manage
 **/ 

// function getAllStudents() {
//     let listTemplate = '';
//     setTimeout(() => {
//         students.forEach((e, i) => {
//             let listItem = `<li>${i+1}: ${e.name}</li>`;
//             listTemplate += listItem;
//         });
//         document.getElementById('list').innerHTML = listTemplate;
//     }, 1000);
// }

// function createStudent(name, callback) {
//     setTimeout(() => {
//         students.push({name});
//         callback();
//     }, 3000);
// }

// createStudent('Amey', getAllStudents);


/**
 * callback way: order of execution can be manage; having callback hell problem
 **/ 

// function getAllStudents(callback) {
//     setTimeout(() => {
//         if (students.length > 0) {
//             callback(null, students);
//         }else{
//             callback('Opsie Empty list!', null);
//         }
//     }, 1000);
// }

// function createStudent(student, callback) {
//     setTimeout(() => {
//         if (student) {
//             students.push(student);
//             callback(null, student);
//         } else{
//             callback('Opsie batch limit exceed!', null);
//         }
//     }, 3000);
// }

// function notify(msg, callback) {
//     setTimeout(() => {
//         if (msg) {
//             callback(null, 'Notification sent! MESSAGE: ' + msg +', ON: ' + new Date().toLocaleString());
//         } else {
//             callback('Opsie failed to notify!', null);
//         }
//     }, 2000);
// }

// // callback hell
// createStudent({name: 'Amey'}, (err, res) => {
//     if (err == null) {
//         getAllStudents((err, res) => {
//             if (err == null) {
//                 // display
//                 let listTemplate = '';
//                 res.forEach((e, i) => {
//                     let listItem = `<li>${i+1}: ${e.name}</li>`;
//                     listTemplate += listItem;
//                 });
//                 document.getElementById('list').innerHTML = listTemplate;

//                 notify(`latest count is ${res.length}`, (err, res) => {
//                     if (err == null) {
//                         document.getElementById('notify-text').innerText = res;
//                     } else{
//                         console.error(err);
//                     }
//                 })
//             } else {
//                 console.error(err);
//             }
//         })
//     } else {
//         console.error(err);
//     }
// });

/**
 * promise way: order of execution can be manage; have readability problem
 **/ 

// function getAllStudents() {
//     return new Promise((resolve, reject) => {
//         let listTemplate = '';
//         setTimeout(() => {
//             if (students.length > 0) {
//                 resolve(students);
//             }else{
//                 reject('Opsie Empty list!');
//             }
//         }, 1000);
//     });
// }

// function createStudent(student) {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             if (student) {
//                 students.push(student);
//                 resolve(student);
//             } else{
//                 reject('Opsie batch limit exceed!');
//             }
//         }, 3000);
//     });
// }

// function notify(msg) {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             if (msg) {
//                 resolve('Notification sent! MESSAGE: ' + msg +', ON: ' + new Date().toLocaleString());
//             } else {
//                 reject('Opsie failed to notify!');
//             }
//         }, 2000);
//     });
// }

// createStudent({name: 'Amey'})
//     .then(res => getAllStudents(), err => console.error('1: ', err))
//     .then(res => {
//         // display
//         let listTemplate = '';
//         res.forEach((e, i) => {
//             let listItem = `<li>${i+1}: ${e.name}</li>`;
//             listTemplate += listItem;
//         });
//         document.getElementById('list').innerHTML = listTemplate;

//         return notify(`latest count is ${res.length}`);
//     }, err => console.error('2: ', err))
//     .then(res => (document.getElementById('notify-text').innerText = res), err => console.error('3: ', err))
//     .catch(err => console.error('4: ', err))

/**
 * async-wait way: order of execution can be manage, readable
 **/ 


// function getAllStudents() {
//     return new Promise((resolve, reject) => {
//         console.log('Inside getAllStudents Promise');
//         let listTemplate = '';
//         setTimeout(() => {
//             if (students.length > 0) {
//                 resolve(students);
//             }else{
//                 reject('Opsie Empty list!');
//             }
//         }, 1000);
//     });
// }

// function createStudent(student) {
//     return new Promise((resolve, reject) => {
//         console.log('Inside createStudent Promise');
//         setTimeout(() => {
//             if (student) {
//                 students.push(student);
//                 resolve(student);
//             } else{
//                 reject('Opsie batch limit exceed!');
//             }
//         }, 3000);
//     });
// }

// function notify(msg) {
//     return new Promise((resolve, reject) => {
//         console.log('Inside notify Promise');
//         setTimeout(() => {
//             if (msg) {
//                 resolve('Notification sent! MESSAGE: ' + msg +', ON: ' + new Date().toLocaleString());
//             } else {
//                 reject('Opsie failed to notify!');
//             }
//         }, 2000);
//     });
// }

// async function driverFn() {
//     try {
//         let createStudentRes = await createStudent({name: 'Amey'});

//         let getAllStudentsRes = await getAllStudents();
        
//         // display
//         let listTemplate = '';
//         getAllStudentsRes.forEach((e, i) => {
//             let listItem = `<li>${i+1}: ${e.name}</li>`;
//             listTemplate += listItem;
//         });
//         document.getElementById('list').innerHTML = listTemplate;
    
//         let notifyRes = await notify(`latest count is ${getAllStudentsRes.length}`);
    
//         document.getElementById('notify-text').innerText = notifyRes;    
//     } catch (err) {
//         console.error(err);
//     }
// }

// driverFn().then().catch();


/**
 * [REFER: https://tusharsharma.dev/posts/js-promises-eager-not-lazy]
 * Javascript Promises are Eager in nature and Not Lazy.
 * 
 * 
 * As soon as the javascript Interpreter sees a promise declaration. 
 * It immediately executes its implementation synchronously. 
 * Even though it will get settled eventually.
 * 
 * [REFER: CODE_SAMPLE_1]
 * See the code below. We didn't have to do a fetchItem.then() to execute that promise. 
 * It started executing as soon as it was declared. 
 * This is one of the major differences between promises and observables. 
 * Observables don't execute their implementation unless they are subscribed.
 * 
 * [REFER: CODE_SAMPLE_2]
 * To avoid this you can wrap your promise inside a function declaration 
 * and call that function only when it is required. 
 * This works because the javascript Interpreter does not go through a function declaration unless 
 * that function is called
 */


// CODE_SAMPLE_1
// console.log('Start');
// const fetchItem = new Promise(resolve => {
//   console.log('Inside Promise');
//   resolve();
// });
// console.log('Finished');

// CODE_SAMPLE_2
// console.log('Start');
// function runPromise() {
//   const fetchItem = new Promise(resolve => {
//     console.log('Inside Promise');
//     resolve();
//   });
//   return fetchItem;
// }
// console.log('Finished');
// runPromise();