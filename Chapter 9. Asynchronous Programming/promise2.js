/*
REAL - TIME EXAMPLE OF PROMISE
consider a sceneria where you and your roommate want to have dinner at home
you want to prepare your special soup. At the same time, you feel like having pasta from the food truck nearby.
you ask your roommate, 'hey can you go down to the food truck and get us some pasta?'. When she is about to leave,
you tell her,
- there is no point in me waiting till you're back to prepare the soup. so I'll start with soup now but when you reach the place,
can you promise taht you will text me so i can start setting up the dinning table?
- Also let me know if something goe wrong. if you can't find the food truck, or if they are out of pasta for the night.
let me know that you can't get the pasta and i'll start cooking some noodles instead.
your frnd says 'sure i promise i'll head out now and text you in sometime'.
now you go about preparing your soup but the status on pasta? we can say that it is currently pending till you recieve 
that msg from your frnd.
- when you get back a text msg saying that she is getting the pasta, your desire to eat pasta has been FULFILLED. You can
then proceed to setup the table.
- If the txt msg says that she can't bring back any pasta, your desire to have pasta has been REJECTED and you now
to cook some noodles instead.

Here,
Roommate - Promise
can get pasta/can not - Promise Value
Can get pasta - Fulfilled promise
cannot get pasta - reject promise
set up the table - success callbak
cook noodles - failure callback
*/
console.log("Start")
let promise= new Promise((resolve, reject)=>{
    setTimeout(()=>{
        //Food truck found
        //Change status from 'pending' to 'fulfilled'
        resolve('Bringing Pasta');
        // //Food truck not found         
        // //Change status from 'pending' to 'rejected'
        // reject('Cannot bring Pasta');
    },2000);
});

const onFullFillment= (result)=>{
    //resolve was called
    console.log(result);
    console.log('Start setting up the table:)');
}

const onRejection= (error)=>{
    //reject was called
    console.log(error);
    console.log('Start preparing noodles:(');
}
promise.then(onFullFillment).catch(onRejection);
console.log("End")