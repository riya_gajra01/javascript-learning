function myDispatcher(some){
    document.getElementById('list').innerHTML= some;
}
let myPromise = new Promise(function(myResolve, myReject) {
    let x=0;
    if(x==0)
    myResolve('OK');
    else
    myPromise('ERROR')
});
myPromise.then(
    function(value){myDispatcher(value);},
    function(error){myDispatcher(error);}
);

// async function getFile(){
//     let myPromise = new Promise(function(myResolve, myReject) {
//     let request= new XMLHttpRequest();
//     request.open('GET', "You-Dont-Know-JS-Async-Performance.pdf");
//     request.onload= function(){
//         if(request.status==200){
//             myResolve(request.response);
//         }else{
//             myReject('Error.' +request.status);
//         }
//     }
//     request.send();
// });
// document.getElementById('demo').innerHTML= await myPromise;
// }
// getFile();