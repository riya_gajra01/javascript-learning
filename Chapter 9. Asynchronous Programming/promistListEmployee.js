const postListPromise = new Promise((resolve, reject) =>{
    $.get('	https://dummy.restapiexample.com/api/v1/employee/1', (data) =>{
        resolve(data);
    }).fail(err =>{
        reject(new Error(`Call Failed to GET LIST OF Employee with status ${err.status}`))
    })
})

postListPromise.then((response) =>{
    console.log('Call Success');
    console.log('The Response => ', response);
}).catch((error) =>{
    console.log('Call Failed');
    console.log('The Error => ', error);  
})