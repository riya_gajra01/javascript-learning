// let a= 1+1;
// let p= new Promise((resolve, reject)=>{
//     // let a= 1+1;
//     if(a==2){
//         resolve('Success.');
//     }else{
//         reject('Failed.');
//     }
// })

// p.then((msg)=>{
//     console.log('Insie then block....\nmsg= ', msg);   
// }).catch((error) => {
//     console.log('Insie catch block....\nmsg= ', error);   
// })


// // callback way:
// const userLeft= true
// const userWatchingTv= true

// function watchTutorialPromise(callback, errorCallback){
//     if(userLeft){
//         errorCallback({
//             name: 'NoUserFound',
//             message: 'Sorry, User Left :('
//         })
//     }else if(userWatchingTv){
//         errorCallback({
//             name: 'WatchingTv',
//             message: 'Learning < Watching Tv'
//         })
//     } else{
//         callback('Welcome to Promise Tutorial')
//     }
// }

// watchTutorialPromise((a) => {
//     console.log('Success: ' + a);   
// }, (b)=>{
//     console.log('Error: '+b.name + '\nError Msg: ' + b.message);  
// })


// // Promise way:
// const userLeft= true
// const userWatchingTv= true

// function watchTutorialPromise(){
// return new Promise((resolve, reject) => { 
//     if(userLeft){
//         reject({
//             name: 'NoUserFound',
//             message: 'Sorry, User Left :('
//         })
//     }else if(userWatchingTv){
//         reject({
//             name: 'WatchingTv',
//             message: 'Learning < Watching Tv'
//         })
//     } else{
//         resolve('Welcome to Promise Tutorial')
//     }
// })   
// }
// watchTutorialPromise().then((message) => {
//     console.log('Success: ' + message);   
// }).catch((error) => { 
//     console.log('Error: '+error.name + '\nError Msg: ' + error.message);  
// })


//Exampple - 3
// here user want to recored all of the three videos parallely so that he don't need to wait. 
const recordVideoOne = new Promise((resolve, reject) => {
    resolve('Video 1 Recorded');
})
const recordVideoTwo = new Promise((resolve, reject) => {
    resolve('Video 2 Recorded');
})
const recordVideoThree = new Promise((resolve, reject) => {
    resolve('Video 3 Recorded');
})

Promise.race([
    recordVideoOne,
    recordVideoTwo,
    recordVideoThree
]).then((msg) => {
    console.log(msg);   
}).catch(()=>{
    console.log('Something went wrong!!! ');
})