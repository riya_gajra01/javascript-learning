function makeRequest(location){
    return new Promise((resolve, reject)=> {
        console.log(`Making request to ${location}`)
        if(location == 'Google')
        resolve('Hey Google')
        else
        reject('For mapping, use Google only')
    })
}
function processRequest(response) {
    return new Promise((resolve, reject)=> {
        console.log(`Processing Response`)
        resolve(`Extra information: ${response}`)    
})
}

makeRequest('Google').then(response => {
    console.log(`Response Received`) 
    return processRequest(response)
}).then(processedResponse =>{
    console.log(processedResponse)
}).catch(error =>{
    console.log(error) 
})