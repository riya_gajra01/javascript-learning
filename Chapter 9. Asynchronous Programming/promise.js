// var data= "Initial State";
// setTimeout(function(){
//     data= "New State";
//  // console.warn("Inside Timer Function");   
// },2000);
// console.warn(data);
// // here we have a function whose replying after sometime. inside this function/API we have lots of data, then it is impossible to
// // get the data from this function bcoz it's taking some time and then replying. For such scenerios we have promise

// var data = new Promise(function(success, fail){
//   setTimeout(function(){
//     success("New State")
//     },2000)
//   })
// // console.warn(data);

// data.then(function(val){
//   console.warn(data);
// })

