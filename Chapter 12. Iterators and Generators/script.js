// iterator 

function numberIterator(arr) {
    var nextNum =0;
   // console.log(arr.length);
    return {
        next(){            
            if(nextNum < arr.length){ 
                console.log('if block');
                return{
                    value : arr[nextNum++],
                    done : false
                }
            }else{
                console.log('else');
                return{
                    value : arr[nextNum++],
                    done : true
                }
            }
        }
    }
}
var number =[1,2,3,4];
let numbers =numberIterator(number);
console.log(numbers.next());
console.log(numbers.next());

