class Range {
    constructor(start, end) {
      this.start = start
      this.end = end
    }

    [Symbol.iterator]() {
      let current = this.start
      let last = this.end
      return {
        next() {
          if (current <= last) {
            const result = { value: current }
            current++
            return result
          } else {
            return { done: true }
          }
        }
      }
   }
 }

const range= new Range(10,20);
console.log([range]);
console.log([...range]);
