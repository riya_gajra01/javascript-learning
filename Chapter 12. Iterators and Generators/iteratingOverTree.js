class BinaryTree {
    constructor(value, left=null, right=null) {
    this.value = value;
    this.left = left;
    this.right = right;
    }
   [Symbol.iterator]() {
    yield this.value;
    }
}
const tree = new BinaryTree('a', new BinaryTree('b', new BinaryTree('c'), new BinaryTree('d')), new BinaryTree('e'));
for (const x of tree) {
console.log(x);
}    