
// //Generator
// function* makeIterator() {
//    let count= [1,2,3,4];
//    for(let i=0; i<count.length; i++){
//     yield count[i];  //yield means pause.
//    }
//    return count
//   }
//   let iterator = makeIterator();
//   console.log(iterator.next());
//   console.log(iterator.next());
//   console.log(iterator.next());
//   console.log(iterator.next());
// //   console.log(iterator.next());
  

//calling generator from another generator
function* outer(){
    yield 1;
    // yield* inner();
    inner();
    yield 2;
}
function inner(){
    console.log( 'a');
    //  'a';
    // yield 'b';
}
var generator= outer();
console.log( generator.next());
console.log( generator.next());
console.log( generator.next());
console.log( generator.next());
console.log( generator.next());
