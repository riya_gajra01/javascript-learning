function* gen(){
    yield func()
    return 
}
function func(){
    return "Function call"
}
const generator= gen();
const gVal1= generator.next();
console.log('Generator first value: ', gVal1);
const gVal2= generator.next();
console.log('Generator second value: ', gVal2);