 let num = 123456.78
let result = num.toLocaleString('de')
console.log(result);
result = num.toLocaleString('ar')
console.log(result);
result = num.toLocaleString('ko')
console.log(result);
result = num.toLocaleString('en')
console.log(result);


let formatter = new Intl.NumberFormat('de');
result = formatter.format(num);
console.log(result);
result=formatter.formatToParts(num);
console.log(result);

//Intl.NumberFormate
const number = 123456778.789;
console.log(new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number));
// expected output: "123.456.778,79 €"

// the Japanese yen doesn't use a minor unit
console.log(new Intl.NumberFormat('ja-JP', { style: 'currency', currency: 'JPY' }).format(number));
// expected output: "￥123,456,779"

// limit to three significant digits
console.log(new Intl.NumberFormat('en-IN', { style: 'currency', currency: 'INR' , maximumSignificantDigits:4, maximumFractionDigits:2 }).format(number));
// expected output: "1,23,000"

// Formatting with units
console.log(new Intl.NumberFormat('pt-PT',  { style: 'unit', unit: 'kilometer-per-hour' }).format(50));
// expected output:  50 km/h

console.log((1).toLocaleString('en-IN', { style: 'unit', unit: 'liter', unitDisplay: 'long',}));
// expected output:  1 liter

const collator = new Intl.Collator('en-US-u-kn-true', { sensitivity: 'base' })
console.log(collator.resolvedOptions());