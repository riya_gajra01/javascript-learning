 let newYearsEve = new Date(1999, 11, 31, 23, 59);
 console.log(newYearsEve);
 newYearsEve.toLocaleString('de'); //de - German - Deutsch
 console.log(newYearsEve);
 newYearsEve.toLocaleString('en', { timeZone: 'Asia/Kolkata' });
 console.log(newYearsEve);


//default format of toString method
console.log("\n--------------------------");
console.log("Local time: "+new Date());

//default locale of browser, which is crome
console.log("--------------------------");
console.log("Locale of browser: "+new Date().toLocaleString());

//Indian time zone
console.log("India : "+new Date().toLocaleString('en-IN'));

//British English formate i.e. dmyyyy along with 24-hour timezone without AM/PM
console.log("British : "+new Date().toLocaleString('en-GB', {timeZone: 'UTC'})); 
//here en is language code and GB is ountry or region

//US English formate i.e. m-d-yyy along with 12-hour timwzone with AM/PM
console.log("USA : "+new Date().toLocaleString('en-US', {timeZone: 'UTC'}));

//German
console.log("German : "+new Date().toLocaleString('de-DE'));

//korean
console.log("korean : "+new Date().toLocaleString('ko-KR'));

//Arabic
console.log("Arabic : "+new Date().toLocaleString('ar-EG'));

