// The Intl object is the namespace for the ECMAScript Internationalization API, 
// which provides language sensitive string comparison, number formatting, and date and time formatting. 
// The Intl object provides access to several constructors as well as functionality common to 
// the internationalization constructors and other language sensitive functions.

// Intl.Collator()
// Constructor for collators, which are objects that enable language-sensitive string comparison.
// To use a collator, we can construct a Collator object and then use its compare method.
// The compare method does a comparison of the alphabetical order of the entire string based on the locale. 
// For example, if we want to compare two strings in the German using its alphabet’s order, we can write the following code:

const collator = new Intl.Collator('de');
const order = collator.compare('Ü', 'ß');
console.log(order);

// Then a number is returned from the compare function. 
// 1 is returned if the string in the first parameter comes after the second one alphabetically, 
// 0 if both strings are the same, and 
// -1 is returned if the string in the first parameter comes before the second string alphabetically.
console.log(new Intl.Collator().compare('a', 'c')); // → a negative value
console.log(new Intl.Collator().compare('c', 'a')); // → a positive value
console.log(new Intl.Collator().compare('a', 'a')); // → 0
let c = new Intl.Collator('en');
const sortedLetters = ['Z', 'Ä', 'Ö', 'Ü', 'ß'].sort(c.compare);
console.log(sortedLetters);
c = new Intl.Collator('en-ca-u-kf-upper');
const sorted = ['Able', 'able'].sort(collator.compare);
console.log(sorted);

let languageCodes=['en','pt','fr','cn','ru','sv'];
console.log(Intl.Collator.supportedLocalesOf(languageCodes));
console.log(new Intl.Collator('sv', {
    sensitivity: "variant",
    ignorePunctuation: true,
    numeric: true,
    caseFirst: false
}).compare('a','A'))