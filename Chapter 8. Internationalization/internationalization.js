var rtf1= new Intl.RelativeTimeFormat('en', { style: 'short'});
console.log(rtf1.format(1, 'day'));

var rtf1= new Intl.RelativeTimeFormat('fr', {style: 'narrow'});
console.log(rtf1.format(0, 'day'));

//Here, RelativeTimeFormat is the more understable/human redable format of time
const timeFormat= new Intl.RelativeTimeFormat('en', {
    localeMatcher: "lookup",
    numeric: "auto",
    style: "short"
}) 
// here we have set to languge= en, we have our locale manager set to lookup which means if the language isn't found, this is
//going to default back to system language.
// numeric means it will represent things in numeric format
// style used to set a complete word or its abbrivation.
console.group("Days");
console.log(timeFormat.format(-1, 'day'));
console.log(timeFormat.format(0, 'day'));
console.log(timeFormat.format(1, 'day'));
console.log(timeFormat.format(2, 'day'));
console.groupEnd();