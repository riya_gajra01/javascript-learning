const date = new Date();
// get a Date object with the specified Time zone
function changeTimeZone(date, timeZone) {
  if (typeof date === 'string') {
    return new Date(
      new Date(date).toLocaleString('en-IN', {
        timeZone,
      }),
    );
  }
  return new Date(
    date.toLocaleString('en-US', {
      timeZone,
    }),
  );
}
//America/Los_Angeles
const laDate = changeTimeZone(new Date(), 'Asia/Kolkata');
console.log(laDate); // 👉️ "Sun Jan 15 2022 11:54:44"

