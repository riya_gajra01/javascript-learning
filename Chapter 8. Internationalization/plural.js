dollars = { one: 'dollar', other: 'dollars' }
rubles = { one: 'рубль', few: 'рубля', many: 'рублей' }
let result = dollars[i=> new Intl.PluralRules('en').select(i)];
console.log(result);
// rubles[new Intl.PluralRules('ru').select(i)]


arr=[0, 1, 2, 3, 4, 5];
result=arr.map(i => (new Intl.PluralRules('en').select(i)));
  console.log(result);
  // ['other', 'one', 'other', 'other', 'other', 'other']

result=arr.map(i => (new Intl.PluralRules('ru').select(i)));
console.log(result);
  // ['many', 'one', 'few', 'few', 'few', 'many']

  const str = 'Å™'
  result=['NFC', 'NFD', 'NFKC', 'NFKD'].map(mode => [...str.normalize(mode)]);
  // The normalize() method returns the Unicode Normalization Form of the string.
  console.log(result);
// Yields ['Å', '™'], ['A', '°', '™'], ['Å', 'T', 'M'], ['A', '°', 'T', 'M']