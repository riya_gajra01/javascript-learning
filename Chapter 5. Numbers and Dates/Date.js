
//creating date using new Date()- this constructor will create new object of date with current date and time
var now= new Date();
console.log(now); 

//creating date using new Date(millisecond)- creates new date object as January 1, 1970, 00:00:00 Universal Time (UTC)
var now= new Date(864000);
console.log(now);

//creating date using new Date(y m d h min s ms)- creates date obj with th date specified by the details.
// we can omit some of the arguments as well
var now= new Date(2022,6,30,15,50,50,0);
console.log(now);

//creating date using new Date(dateString)- creates date object from given date string
var now= new Date("1 Aug 1998 04:10:20");
console.log(now);