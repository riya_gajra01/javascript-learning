//getMonthName() accept month number as parameter
//getDayName() accept day number as parameter
// month start from 0 to 11, week day starts from 0-6


var date= new Date();
var month= date.getMonth();
var day= date.getDay();
console.log("Month Name: " +getMonthName(month) +"\nDay Name: " +getDayName(day));

function getMonthName(monthNumber){
    var monthName= ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return monthName[monthNumber];
}

function getDayName(dayNumber){
    var dayName= ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    return dayName[dayNumber];
}