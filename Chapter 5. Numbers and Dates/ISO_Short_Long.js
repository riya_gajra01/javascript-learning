//ISO 8601 is the international standard for the representation of dates & times.
var date= new Date("2022-06");
console.log(date);
var date= new Date("2022");
console.log(date);
var date= new Date("2022-06-30T16:05:30Z");
console.log(date);

//Short Date written with MM/DD/YYYY format
var date= new Date("06-30-2022");
console.log(date);

//Long Date written with MMM/DD/YYYY format
//here month and day can be in any order.
var date= new Date("Aug 1 1998");
//var date= new Date("Aug, 1, 1998");
console.log(date);
