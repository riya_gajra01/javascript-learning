console.log(parseInt(68.444));           //68
console.log(parseInt("10 20 30"));       //10
console.log(parseInt(' 3A'));            //3
console.log(parseInt(' A3'));            //NaN
console.log(parseInt('0x3A'));          //58 i.e convert hexa 0x3A into its equivalent decimal number
console.log(parseInt("12",8));          // 10 i.e convert octal 12 into its equivalent decimal number

const intRegex = /^[+-]?[0-9]+$/;
let str='42526546367';
if (intRegex.test(str)) 
   value = parseInt(str)
console.log(value)

const floatRegex = /^[+-]?((0|[1-9][0-9]*)(\.[0-9]*)?|\.[0-9]+)([eE][+-]?[0-9]+)?$/
let string='111.2222'
if (floatRegex.test(string)) 
   val = parseFloat(string)
console.log(val)
