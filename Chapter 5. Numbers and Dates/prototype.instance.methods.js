var num1= 42;
var num2= 0x2A;
var num3= 0o52;
var num4= 0b101010;
console.log(num1.toString(10), num2.toString(10), num3.toString(10), num4.toString(10), (200).toString(16)); // convert object into string
const PI= 3.141596254;
num1= PI.toFixed(3);   //toFixed used to mention number of fractional digits after decimal point.
console.log(num1);  
num2= PI.toPrecision(6); //used to mention number of fractional digits after decimal point by rounding up the last digit.
console.log(num2); 
num3= 1000000;
console.log(num3.toExponential());//used to return exponential form of the given entity.
const numObj= new Number(42);
const num= numObj.valueOf(); //can fetch value from object
console.log(num, typeof num, typeof numObj);