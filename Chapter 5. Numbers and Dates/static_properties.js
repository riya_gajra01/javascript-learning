const biggestNum= Number.MAX_VALUE;
const smallestNum= Number.MIN_VALUE;
const infiniteNum= Number.POSITIVE_INFINITY;
const negInfiniteNum= Number.NEGATIVE_INFINITY;
const notANum= Number.NaN;
const upperRangeInt= Number.MAX_SAFE_INTEGER;
const lowerRangeInt= Number.MIN_SAFE_INTEGER;
console.log(biggestNum,smallestNum,infiniteNum,negInfiniteNum,notANum);
console.log(upperRangeInt, lowerRangeInt);