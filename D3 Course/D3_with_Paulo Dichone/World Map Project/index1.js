


const svg = d3.select('svg');

const projection = d3.geoNaturalEarth1();
const pathGenerator = d3.geoPath().projection(projection);

svg.append('path')
    .attr('class', 'sphere')
    .attr('d', pathGenerator({ type: 'Sphere' }));


// d3.json('https://unpkg.com/world-atlas@1.1.4/world/110m.json')
//     .then(data => {
//         const countries = topojson.feature(data, data.objects.countries);
//         svg.selectAll('path').data(countries.features)
//             .enter().append('path')
//             .attr('class', 'country')

//             .attr('d', pathGenerator)
//             .append('title')
//             .text('Hello')
//     });


Promise.all([
    d3.tsv('https://unpkg.com/world-atlas@1.1.4/world/50m.tsv'),
    d3.json('https://unpkg.com/world-atlas@1.1.4/world/110m.json')
]).then(([tsvData,data])=>{
    const countryName={};

tsvData.forEach(d => {
    countryName[d.iso_n3]=d.name;
});

    const countries = topojson.feature(data, data.objects.countries);
    svg.selectAll('path').data(countries.features)
        .enter().append('path')
        .attr('class', 'country')

        .attr('d', pathGenerator)
        .append('title')
        //.text(d=>console.log(d.id))
        //.text(d=>d.id)
        .text(d=>console.log(countryName[d.id]))
        .text(d=>countryName[d.id])
})


