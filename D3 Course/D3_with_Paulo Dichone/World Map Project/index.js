//topoJson  provides mechnism for generating TopoJson files from Natural
//Earth's vector data

// svg.append('path')
//     .attr('class', 'sphere')
//     .attr('d', pathGenerator({type: 'Sphere'}));


 







    // d3.json('https://unpkg.com/world-atlas@1.1.4/world/110m.json')
    // .then(data => {
    //   const countries = topojson.feature(data, data.objects.countries); //returns the geojson feature or featureCollection for the specified object in given topology
    //  console.log(countries);
    // });


    
    
    const svg = d3.select('svg');
    
    const projection = d3.geoNaturalEarth1();
    const pathGenerator = d3.geoPath().projection(projection);
    
    svg.append('path')
        .attr('class', 'sphere')
        .attr('d', pathGenerator({type: 'Sphere'}));
    
    d3.json('https://unpkg.com/world-atlas@1.1.4/world/110m.json')
      .then(data => {
        const countries = topojson.feature(data, data.objects.countries);
        svg.selectAll('path').data(countries.features)
          .enter().append('path')
            .attr('class', 'country')
            .attr('d', pathGenerator);
      });