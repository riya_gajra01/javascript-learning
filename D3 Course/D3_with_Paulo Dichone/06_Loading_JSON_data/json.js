// var barData = [
//     {width: 25, height: 40, fill: 'green'},
//     {width: 25, height: 10, fill: 'red'},
//     {width: 25, height: 50, fill: 'brown'},
//     {width: 25, height: 30, fill: 'purple'},
//     {width: 25, height: 90, fill: 'gray'}

// ];
// var width = 600
// var height = 200

// var canvas = d3.select('body')
//     .append('svg')
//     .attr('style','border: 3px dashed')
//     .attr('width',width)
//     .attr('height',height)

// var bars = canvas.selectAll('rect');


// bars.data(barData)
//     .enter()
//         .append('rect')
//         .attr('width', 24) //  width = 400, 100, 500, 300, 900, 200
//         .attr('height',(data) => data.height*2)
//         .attr('x', (data, i) => i*25 +5)
//         .attr('y', (data, i) => 200 - (data.height*2))
//         .attr('fill', (color) => color.fill)
//         .attr('stroke', 'plum')
//         .attr('stroke-width',3);

var width = 600
var height = 300

var canvas = d3.select('body')
    .append('svg')
    .attr('style','border: 3px dashed')
    .attr('width',width)
    .attr('height',height)

var bars = canvas.selectAll('rect');

const promise = d3.json('./barData.json')

promise.then(json => {
        console.log(json)
        bars.data(json)
            .enter()
            .append('rect')
            .attr('width', 24)
            .attr('height',(data) => data.height*2)
            .attr('x', (data, i) => i*25 +5)
            .attr('y', (data, i) => 200 - (data.height*2))
            .attr('fill', (color) => color.fill)
            .attr('stroke', 'plum')
            .attr('stroke-width',3);   
})

