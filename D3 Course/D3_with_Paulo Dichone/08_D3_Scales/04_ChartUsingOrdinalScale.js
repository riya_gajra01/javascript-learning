d3.json('eventDates.json')
  .then((data) => {
    const svg = d3.select('svg#example3');
    const parseTime = d3.timeParse('%B %d, %Y');

    const dates = [];
    for (const obj of data) {
      dates.push(parseTime(obj.date));
    }
    
    let domain = d3.extent(dates);
    domain = [d3.timeYear.floor(dates[0]), d3.timeYear.ceil(domain[1])];

    const xScale = d3.scaleTime()
      .domain(domain)
      .range([25, 555]);

    const xAxis = d3.axisBottom(xScale)
      .ticks(d3.timeYear);

    svg.append('g')
      .attr('transform', 'translate(0,70)')
      .call(xAxis);

    const years = d3.map(dates, (d) => d.getFullYear()).keys();

    const colorScale = d3.scaleOrdinal()
      .domain(years)
      .range(['#000000', '#A4A4A4', '#1DA1F2', '#810081']);

    svg.selectAll('rect')
      .data(data)
      .enter()
      .append('rect')
      .attr('x', (d) => xScale(parseTime(d.date)))
      .attr('y', 40)
      .attr('width', 10)
      .attr('height', 30)
      .attr('fill', (d) => colorScale(parseTime(d.date).getFullYear()));
  });