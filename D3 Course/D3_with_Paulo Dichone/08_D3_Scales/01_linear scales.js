d3.json('monthlySales.json')
  .then((data) => {
    const xScale = d3.scaleLinear()
      .domain([1, 10])
      .range([25, 475])
    const yScale = d3.scaleLinear()
      .domain(d3.extent(data, d => d.sales)) 
      .range([375, 25]) 

    d3.select('#example-0')
      .style('border', 'solid 1px')
      .selectAll('circle')
      .data(data)
      .enter()
      .append('circle')
      .attr('r', '10px')
      .attr('fill', '#598')
      .attr('cx', d => xScale(d.month))
      .attr('cy', d => yScale(d.sales))
    
    const monthScale = d3.scaleOrdinal()
      .domain([1, 12]) 
      .range(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])

    d3.select('#example-0')
      .style('border', 'solid 1px')
      .selectAll('text')
      .data(data)
      .enter()
      .append('text')
      .text(d => monthScale(d.month)) // Use monthScale() here to turn the month into a name
      .attr('x', d => xScale(d.month))  // Use the xScale to position the name on the x
      .attr('y',  d => yScale(d.sales))

  })