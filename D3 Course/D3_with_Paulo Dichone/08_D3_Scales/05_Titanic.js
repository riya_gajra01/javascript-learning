d3.json('titanic-passengers.json')
  .then((data) => {
    const passengers = data.map(p => p.fields)

    // define x linear Scale to position passengers on the x 
    const xScale = d3.scaleLinear()
      .domain([0, passengers.length])
      .range([3, 897])
      .nice();
    
    // Define a linear scale to position passengers on the y based on fare
    const yScale = d3.scaleLinear()
      // .domain([d3.min(passengers.map(p => p.fare)), d3.max(passengers.map(p => p.fare))])
      .domain(d3.extent(passengers, p => p.fare)) // use extent to get min and max
      .range([97, 3])

    // Make an ordinal scale for embarked
    const portScale = d3.scaleOrdinal()
      .domain(['S', 'C', 'Q', 'undefined'])
      .range(['tomato', 'lime', 'gold', 'gray'])

    // Make an oridinal scale for survived
    const survivedScale = d3.scaleOrdinal()
      .domain(['Yes', 'No'])
      .range(['0.5', '0.15'])

    const ageScale = d3.scaleSqrt()
      .domain([d3.min(passengers.map(p => p.age)), d3.max(passengers.map(p => p.age))])
      .range([1, 10])

    d3.select('#titanic-data')
      .style('background-color', '#111')
      .selectAll('rect')
      .data(passengers)
      .enter()
      .append('rect')
      .attr('width', p => 1)
      .attr('height', p => ageScale(p.age))
      .attr('x', (p, i) => xScale(i))
      .attr('y', (p, i) => yScale(p.fare))
      .attr('fill', p => portScale(p.embarked))
      .attr('opacity', p => survivedScale(p.survived))
  })