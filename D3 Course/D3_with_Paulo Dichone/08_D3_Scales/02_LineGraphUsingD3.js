/* Example 1: Drawing a Line Graph using D3 Linear Scale */
const chartHeight = 400;
const chartWidth = 500;
const padding = 25;

d3.json('monthlySales.json')
  .then((data) => {

    const xScale = d3.scaleLinear()
      .domain( d3.extent(data, d => d.month) ) // d3.extent() to find the min and max
      .range([0, chartWidth]) 

    const yScale = d3.scaleLinear()
      .domain([d3.min(data, (d) => d.sales), d3.max(data, (d) => d.sales)])
      .range([chartHeight, 10])

    const lineFunction = d3.line() // d3.line().x().y().curve()
      .x(val => xScale(val.month)) // Use the Scale to get the x 
      .y(val => yScale(val.sales)) // Use the scale to get the y
      .curve( d3.curveLinear ); 

    d3.select('#example1')
      .selectAll('path')
      .data(data)
      .enter()
      .append('path')
      .attr('d', lineFunction(data))   // An svg line is made up data. Use the linefunction to get the data
      .attr('stroke', 'orange')
      .attr('stroke-width', 4)
      .attr('fill', 'none');

    d3.select('#example1')
      .style('border', 'solid 1px')
      .selectAll('text')
      .data(data)
      .enter()
      .append('text')
      .text(val => val.sales)
      .attr('x', val => xScale(val.month))
      .attr('y', val => yScale(val.sales))
      .attr('font-size', '12px')
      .attr('fill', '#666666')
  });

