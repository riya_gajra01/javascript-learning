d3.json('monthlySales.json')
  .then((data) => {
  
    const xScale = d3.scaleLinear()
      .domain([1, 10])
      .range([25, 475])
      .nice();

    const yScale = d3.scaleLinear()
      .domain(d3.extent(data, d => d.sales))
      .range([375, 50])
      .nice();

    const xAxisGen = d3.axisBottom(xScale);
    const yAxisGen = d3.axisLeft(yScale).ticks(4);

    const lineFunction = d3.line()
      .x((val) => xScale(val.month))
      .y((val) => yScale(val.sales))
      // .curve(d3.curveBasis)
      // .curve( d3.curveLinear );

    const svg = d3.select('svg#example2');
    svg.append('g')
      .call(yAxisGen)
      .attr('class', 'axis')
      .attr('transform', `translate(${padding},0)`);
    svg.append('g')
      .call(xAxisGen)
      .attr('class', 'axis')
      .attr('transform', `translate(0,${chartHeight - padding})`);
    svg.append('path')
      .attr('d', lineFunction(data))
      .attr('stroke', 'purple')
      .attr('stroke-width', 2)
      .attr('fill', 'none');
    d3.select('svg#example2')
      .selectAll('text')
      .data(data)
      .enter()
      .append('text')
      .text((val) => val.sales)
      .attr('x', val => xScale(val.month))
      .attr('y', val => yScale(val.sales))
      .attr('fill', '#666666')
      .attr('text-ancchor', 'start')
      .attr('dy', '.35em');
  });