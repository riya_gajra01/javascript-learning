// console.log(d3);

/*
d3.select();  --> returns first selection of dom element matching the criteria given
d3.selectAll(); --> returns all the element matching the criteria.
Both of the above methodes accept css selectors or name of DOM element as parameter and return selection of the elemet.
*/

// d3.select('h1')
// .style('color', 'green')
// .attr('class', 'heading')
// .text('Updated Heding from Index.js');

d3.select('div')
.selectAll('p')
.data([100, 22, 3])
.enter()
.append('p')
.text(dataPointValue => dataPointValue);
// /*
// Here in the above line, select div means select division,
// from that division select all paraghraphs. 
// since I have no paragraphs in this division, I'm binding 3 array elements 1,2,3.
// means data() methods binding the data on given selection. Means we can bind elements which dont exist yet to data
// so that we automatically generate them when that data is there. So that once we set up this binding we can enter data by calling enter().
// Because of this enter method, any paragraph enterd into <div> will be treated as datapoint.
// */

// d3.select('body').append('p').text('d3 stands for Data Driven Document.'+'\nDocument is any web document i.e. HTML(HyperText Markup Language) page, CSS(Cascading Style Sheets) or SVG(Scalable Vector Graphics).'+'\nData is information provided by user.'+'This data is driving the document. D3 is used to join this data into document i.e. html elements.');
// d3.select('body').append('p').text('d3 stands for Data Driven Document.'+'\nDocument is any web document i.e. HTML(HyperText Markup Language) page, CSS(Cascading Style Sheets) or SVG(Scalable Vector Graphics).'+'\nData is information provided by user.'+'This data is driving the document. D3 is used to join this data into document i.e. html elements.');
// d3.select('body').selectAll('p').style('color','yellow');

