(function (React,d3,ReactDOM) {
    'use strict';
  
    var React__default = 'default' in React ? React['default'] : React;
    ReactDOM = ReactDOM && ReactDOM.hasOwnProperty('default') ? ReactDOM['default'] : ReactDOM;
  
    const colorScale = d3.scaleOrdinal()
      .domain(['apple', 'lemon'])
      .range(['#c11d1d', '#eae600']);
  
    const radiusScale = d3.scaleOrdinal()
      .domain(['apple', 'lemon'])
      .range([50, 30]);
  
    const xPosition = (d, i) => i * 120 + 60;
  
    const FruitBowl = props => {
      const {
        fruits,
        height,
        onClick,
        selectedFruit,
        setSelectedFruit
      } = props;
      
      return fruits.map((fruit, i) => (
        React__default.createElement( 'circle', {
          key: fruit.id, cx: xPosition(fruit, i), cy: height / 2, r: radiusScale(fruit.type), fill: colorScale(fruit.type), strokeWidth: 5, stroke: fruit.id === selectedFruit
              ? 'black'
              : 'none', onClick: () => { setSelectedFruit(fruit.id); } })
      ));
    };
  
    const width = 960;
    const height = 500;
  
    const makeFruit = type => ({
      type,
      id: Math.random()
    });
  
  
    const App = () => {
  
      const [fruits] = React.useState(
        d3.range(5).map(() => makeFruit('apple'))
      ); 
        
      const [
        selectedFruit,
        setSelectedFruit
      ] = React.useState(fruits[0].id);
      
      return (
        React__default.createElement( 'svg', { width: width, height: height },
          React__default.createElement( FruitBowl, {
            fruits: fruits, height: height, selectedFruit: selectedFruit, setSelectedFruit: setSelectedFruit })
        )
      );
    };
  
    const rootElement = document.getElementById('root');
    ReactDOM.render(React__default.createElement( App, null ), rootElement);
  
  
  
  }(React,d3,ReactDOM));