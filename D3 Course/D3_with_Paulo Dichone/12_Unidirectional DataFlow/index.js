import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { range } from 'd3';
import { FruitBowl } from './fruitBowl';

const width = 960;
const height = 500;

const makeFruit = type => ({
  type,
  id: Math.random()
});


const App = () => {

  const [fruits] = useState(
    range(5).map(() => makeFruit('apple'))
  ); 
    
  const [
    selectedFruit,
    setSelectedFruit
  ] = useState(fruits[0].id);
  
  return (
    <svg width={width} height={height}>
      <FruitBowl
        fruits={fruits}
        height={height}
        selectedFruit={selectedFruit}
        setSelectedFruit={setSelectedFruit}
      />
    </svg>
  );
};

const rootElement = document.getElementById('root');
ReactDOM.render(<App />, rootElement);