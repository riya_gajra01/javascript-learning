const svg = d3.select('svg');

const width = +svg.attr('width');
const height = +svg.attr('height');

    
const g = svg.append('g')
    .attr('transform',`translate(${width/2},${height/2})`);

const circle = g.append('circle')
    .attr('r', height/2)  
    .attr('fill','yellow')
    .attr('stroke','black')
    .attr('stroke-width',5);

const eyeSpacing = 100;
const eyeOffSet = -70;
const eyesRadius = 30;
const eyebrowWidth= 50;
const eyebrowHeight= 15;
const eyebrowOffSet = -70;

const eyesGroup = g.append('g')
    .attr('transform',`translate(0,${eyeOffSet})`);

const leftEye = eyesGroup.append('circle')
    .attr('r', eyesRadius)
    .attr('cx', - eyeSpacing)
   
const rightEye = eyesGroup.append('circle')
    .attr('r', eyesRadius)
    .attr('cx', eyeSpacing)
    
const leftEyebrow = eyesGroup
  .append('rect')
    .attr('x',-eyeSpacing - eyebrowWidth/2)
    .attr('y', eyebrowOffSet)
    .attr('width',eyebrowWidth)
    .attr('height',eyebrowHeight)
  .transition().duration(2000)
    .attr('y',eyebrowOffSet - 50)
  .transition().duration(2000)
    .attr('y',eyebrowOffSet) 

const rightEyebrow = eyesGroup
  .append('rect')
    .attr('x',eyeSpacing - eyebrowWidth/2)
    .attr('y', eyebrowOffSet)
    .attr('width',eyebrowWidth)
    .attr('height',eyebrowHeight)
  .transition().duration(2000)
    .attr('y',eyebrowOffSet - 50)
  .transition().duration(2000)
    .attr('y',eyebrowOffSet) 
    
  var arc = d3.arc()
    .innerRadius(150)
    .outerRadius(170)
    .startAngle(4.3)
    .endAngle(2);

  const mouth = g
    .append('path')
    .attr('d', arc)
    .attr('fill','black');
    