// CREATING D3 CANVAS AND CIRCLE
const canvas= d3.select(".canvas");
// console.log(canvas);

//ADD SVG ELEMENT INTO CANVAS.
const svg= canvas.append("svg")
    .attr('width','1200')
    .attr('height','400');

    // CIRCLE
svg.append('circle')
    .attr('cx', 300)
    .attr('cy', 100)
    .attr('r', 50)
    .attr('stroke','yellow')
    .attr('stroke-width',3);
svg.append('text')
    .text('<Circle> - click me')
    .attr('x', 230)
    .attr('y', 165)
    .attr('font-weight','bold')
    .style('fill', 'blue')
    .on("click", function() { window.open("https://www.w3.org/TR/SVG2/shapes.html#CircleElement"); });

    // RECTANGLE
svg.append('rect')
    .attr('width',1198)
    .attr('height',398)
    .attr('x',1)
    .attr('y',1)
    .attr('fill','none')
    .attr('stroke','blue')
    .attr('stroke-width',7);

svg.append('rect')
    .attr('width',400)
    .attr('height',200)
    .attr('x',400)
    .attr('y',100)
    .attr('fill','orange')
    .attr('stroke','black');
svg.append('text')
    .text('<Rectangle> - click me')
    .attr('x', 550)
    .attr('y', 320)
    .attr('font-weight','bolder')
    .style('fill', 'blue')
    .on("click", function() { window.open("https://www.w3.org/TR/SVG2/shapes.html#RectElement"); });

    // LINE 
svg.append('line')
    .attr('x1', 100)
    .attr('x2', 200)
    .attr('y1', 300)
    .attr('y2', 150)
    .attr('stroke','green')
    .attr('stroke-width',7)
svg.append('text')
    .text('<Line> - click me')
    .attr('x', 60)
    .attr('y', 315)
    .attr('font-weight','bolder')
    .style('fill', 'blue')
    .on("click", function() { window.open("https://www.w3.org/TR/SVG2/shapes.html#LineElement"); });

// ROUNDED RECTANGLE
svg.append('rect')
    .attr('width',180)
    .attr('height',100)
    .attr('x',10)
    .attr('y',15)
    .attr('rx',25)
    .attr('fill','navy')
    .attr('stroke','pink')
    .attr('stroke-width',7)
svg.append('text')
    .text('<Rounded Rectangle> - click me')
    .attr('x', 5)
    .attr('y', 135)
    .attr('font-weight','bolder')
    .style('fill', 'blue')
    .on("click", function() { window.open("https://www.tutorialscampus.com/html5/svg-draw-rounded-rectangle.htm"); });

