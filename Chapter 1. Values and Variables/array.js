// for 
let arr = [3 , 6, 2, 56, 32, 5, 89, 32];
let max=arr[0];
for(let i=0; i<arr.length; i++){
    if(arr[i]>max){
        max= arr[i];
    }
}
console.log("Max: "+max);

//for of
let arr1 = [1,2,3,4,7,5];
let max1=arr1[0];
for(const element of arr1)
{
    if(element>max1){
        max1=element;
    }
}
console.log("Max: "+max1);

//for in
let arr2 = [7,56,83,99,54,3];
let max2=arr2[0];
for(let element in arr2)
{
    if(arr2[element]>max2){
        max2=arr2[element];
    }
}
console.log("Max: "+max2);
