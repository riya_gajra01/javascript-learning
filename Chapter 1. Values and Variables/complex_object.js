
let person={
    name: 'Riya',
    firm: 'nabla',
    desg: 'Java Intern',
    'work exp': '3 mnth',
    office_Laptop: {
        cpu: 'i7',
        ram: 8,
        brand: 'Dell'
    }
}  // here we have declared object inside object hence it is complex object

console.log(person) //complete object
console.log(person.office_Laptop) //entire laptop object
console.log(person.office_Laptop.brand)  //brand property of laptop property
console.log("Length: "+person.office_Laptop.brand.length)  //will print length of brand value
console.log()
// console.log("Length: "+person.office_Laptop.brand1)  //no property with brand1 name so print undefined
// console.log("Length: "+person.office_Laptop.brand1?.length)  //no property with brand1 name so throw error
// console.log("Length: "+person.office_Laptop.brand?.length)  //will first check for the property with name brand1 coz of ? then check legth if available.

delete person.office_Laptop.ram
console.log(person.office_Laptop)
console.log()

delete person.office_Laptop
console.log(person)