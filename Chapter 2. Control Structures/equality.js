console.log("\'35\'==\'35\': "+('35'=='35')) //string== string
console.log("\'35\'==35\': "+('35'==35)) //string == number 
//here '35' get converted into number then comparison
console.log("\'35\'===\'35\': "+('35'==='35')) //=== checks equality of value & datatype
console.log("\'35\'===35: "+('35'===35))  // value is equal but datatype is diff
console.log("0==\'': "+(0=='')) 
console.log("0===\'': "+(0==='')) 
//any comparison involving NaN is false
console.log('Hello' <= 5)//If one operand is a number, the other operand is converted to a number.
let i=1;
console.log(i+1===2)