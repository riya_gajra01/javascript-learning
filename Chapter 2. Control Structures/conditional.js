let num1=4;
let num2=15;
let num3=6;
let result;

//If block
result=num1>num2;
if(result)
   console.log("Num1 is greatest...");

console.log("--------*-----------"); 

//If else block
result=num1>num2;
if(result)
   console.log("Num1 is greatest...");
else
   console.log("Num2 is greatest...");

console.log("--------*-----------");

//Else if block
if(num1>num2 && num1>num3)
    console.log("Num1 is greatest...");
else if(num2>num3)
     console.log("Num2 is greatest...");
     else
     console.log("Num3 is greatest...");
     
console.log("--------*-----------");
