let a=3, b=4;
console.log("3>4= "+(3>4));
//Any comparison with NaN results false
console.log("Nan>4= "+(NaN>4)); 
console.log("Nan<4= "+(NaN<4));
console.log("Nan>Nan= "+(NaN>NaN));
console.log("Nan<Nan= "+(NaN<NaN));
//comparing string
console.log("\'Riya\' < \'Gajra\': "+ ('Riya'<'Gajra'))  //82 < 71
console.log("\'Riya\' < \'riya\': "+ ('Riya'<'riya'))  //82 < 114
console.log("\'Riya\' < \'Rita\': "+ ('Riya'<'Rita'))  //y < t
console.log("\'Rita\' < \'Riya\': "+ ('Rita'<'Riya'))  //t < t