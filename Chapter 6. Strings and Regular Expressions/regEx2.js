let reg= /RegEx/;  //g i.e global flag // i i.e. case insensitive
console.log(reg);
console.log(reg.source);

//functions to match expressions.
let s= "Learning RegEx i.e. Regular Expression. following are the RegEx functions"

//1. exec(): return array for match or null for no match.

let result= reg.exec(s);
console.log(result);
// console.log(result.index);
//  result= reg.exec(s);
// console.log(result);
//  result= reg.exec(s);
//  console.log(result);  

//2. test(): returns true or false. test regex present in given content.
result= reg.test("This is test function of Regex");
 console.log(result);

//3. match(): return array for match or null for no match.
result=  s.match(reg);
 console.log(result);

//4. search(): returns index of first match else -1.

// result= s.search(reg);
// console.log(result);

//5. replace(): returns new replaced string with all the replacements.(if no flag is given, first match will be replaced only)
// result= s.replace(reg, "Reg-Ex")
// console.log(result);
