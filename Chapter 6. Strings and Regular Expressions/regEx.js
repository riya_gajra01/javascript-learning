//regular expression is used for pattern matching or string matching
//basically used for email validation or password validation

//pattern to check its a mobile number: should start with 7,8 or 9. and contains total 10 digits.
//  let mobNo= /[789][0-9]{9}$/
// console.log(mobNo.test(5262019264))


//regEx to name: Should start with uppercase, contains lower case alphabets, only one digit is allowed.
let name=/[A-Z][a-z]*[0-9]?[a-z]$/
console.log(name.test("Riy5a"))

//regEx fotoo validate email id having two parts i.e id, domail name.
// id should start with alphabets only & can have alphabets, numbers, [_ - . symbols ].
// after id i want @ symbol followed by domain name having lowercase alphabets only
//after domain i want . which is to be followed by high level domain having lowercase alphabets.
// let email= /[a-z][a-z0-9_\-\.]+[@][a-z]+[\.][a-z]{2,3}/
// email= /^[\w]+([\.-_]?\w+)*@\w+([\.-_]?\w+)*(\.\w{2,3})+$/
// console.log(email.test("123Riabhanushali03@gmail.com"))

// let regEx=/[a-z]/gi
// console.log(regEx.test('RIYA'));
