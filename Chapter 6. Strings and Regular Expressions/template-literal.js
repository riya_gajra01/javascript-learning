//template literals or strings allow embedded expressions. we can use multi line string & string interpolation feature.
// uses ` ` to store the string 

//multi-line 
var str1="Riya"
var str2='Mukesh Gajra'
var str3=`Riya Mukesh Gajra`   //template literals
console.log(str1+"\n"+str2+"\n"+str3)

//string interpolation
//template literals can contain placeholders. indicated by $ and {}. can use var, expression or function with ${}.
console.log(str1+" Mukesh Gajra")
//console.log(`${str1} Mukesh Gajra`)